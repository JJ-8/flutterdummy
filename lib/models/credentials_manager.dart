import 'package:shared_preferences/shared_preferences.dart';

class CredentialsManager {
  String _username;
  String _sessionId;
  int _userId;
  bool _admin;
  Future<SharedPreferences> prefsFuture = SharedPreferences.getInstance();

  CredentialsManager() {
    prefsFuture.then((prefs) {
      _username = prefs.getString('username');
      _sessionId = prefs.getString('sessionId');
      _userId = prefs.getInt('userId');
      _admin = prefs.getBool('admin');
    });
  }

  String getUsername() => _username;
  String getSessionId() => _sessionId;
  int getUserId() => _userId;
  bool getAdmin() => _admin;

  void setUsername(String newusername) {
    _username = newusername;
    prefsFuture.then((prefs) {
      prefs.setString('username', newusername);
    });
  }

  void setSessionId(String newsessionId) {
    _sessionId = newsessionId;
    prefsFuture.then((prefs) {
      prefs.setString('sessionId', newsessionId);
    });
  }

  void setUserId(int newuserId) {
    _userId = newuserId;
    prefsFuture.then((prefs) {
      prefs.setInt('userId', newuserId);
    });
  }

  void setAdmin(bool admin) {
    _admin = admin;
    prefsFuture.then((prefs) {
      prefs.setBool('admin', admin);
    });
  }
}
