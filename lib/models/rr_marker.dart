import 'dart:convert';
import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

class RRMarker extends SymbolOptions{
  String title;
  String subtitle;
  LatLng position;
  DateTime time;
  RRMarkerType type;
  double iconSize;
  List<String> imagePaths;
  String contentFilePath;

  RRMarker(
      {@required this.title,
      this.subtitle,
      @required this.position,
      this.time,
      @required this.type,
      this.iconSize = 0.4,
      this.imagePaths,
      this.contentFilePath});

  Map toJson() => {
    'title': title,
    'subtitle': subtitle ?? "",
    'lat': position.latitude,
    'lon': position.longitude,
    'time': time?.toIso8601String() ?? "",
    'type': type.toString(),
    'iconSize': iconSize,
    'images': jsonEncode(imagePaths) ?? "",
    'contentFilePath': contentFilePath ?? ""
  };

  static RRMarkerType getMarkerTypeFromString(String string) {
    for (final rrmarkertypevalue in RRMarkerType.values) {
      if (string == rrmarkertypevalue.toString()) {
        return rrmarkertypevalue;
      }
    }
  }

  RRMarker.fromJson(Map<String, dynamic> json)
      :title = json["title"],
      subtitle = json["subtitle"] == "" ? null : json["subtitle"],
      position = LatLng(double.parse(json["lat"].toString()), double.parse(json["lon"].toString())),
      time = json["time"] == "" ? null : DateTime.parse(json["time"]),
      type = getMarkerTypeFromString(json["type"]),
      iconSize = double.parse(json["iconSize"].toString()),
      imagePaths = json["images"] == "null" ? null : List.from(jsonDecode(json["images"])),
      contentFilePath = json["contentFilePath"] == "" ? null : json["contentFilePath"];




}

enum RRMarkerType { POI, Hotel, User }
