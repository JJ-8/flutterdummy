import 'package:flutter/material.dart';
import 'package:flutterdummy/services/notification_history_service.dart';

class NotificationsPage extends StatefulWidget {
  @override
  NotificationsPageState createState() => NotificationsPageState();

}

class NotificationsPageState extends State<NotificationsPage> {

  NotificationsHistoryService _notificationsHistoryService;

  @override
  Widget build(BuildContext context) {
    _notificationsHistoryService = NotificationsHistoryService(context);
    return RefreshIndicator(
      child: FutureBuilder(
        future: _notificationsHistoryService.getNotifications(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data != null) {
              return _notificationsHistoryService.buildNotificationList();
            } else {
              return Center(
                child: Text("Herlaad deze pagina a.u.b"),
              );
            }
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ), onRefresh: () async {
        setState(() {
          build(context);
        });
      },
    );
  }


}