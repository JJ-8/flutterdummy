import 'dart:async';
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutterdummy/models/chat_message_model.dart';
import 'package:flutterdummy/services/chat_service.dart';

class ChatPage extends StatefulWidget {
  // String because it can also be 'globalchat' or 'photoboard'
  final String teamId;

  ChatPage({@required this.teamId});

  @override
  ChatPageState createState() => ChatPageState();
}

class ChatPageState extends State<ChatPage> with AfterLayoutMixin<ChatPage> {
  List<ChatMessage> messages = [];

  int _loadedDays = 1;

  final _chatScrollController = ScrollController();
  final _chatBoxController = TextEditingController();

  ChatService _chatService;

  @override
  Widget build(BuildContext context) {
    // build gets called multiple times, but by then, _chatService should already be initialised
    // As as workaround, _chatService only gets initialised
    if (_chatService == null) {
      _chatService =  ChatService(context: context, teamId: widget.teamId);
    }

    return Column(
      children: <Widget>[
        Expanded(
          child: StreamBuilder(
            stream: _chatService.stream,
            builder: (context, snapshot) {
              // This check is in place, because it seems like this builder
              // will also be triggered by a rebuild (using setState, for example)
              // In that case, it'll emit a snapshot with data == null
              if (snapshot.data != null) {
                messages.add(snapshot.data as ChatMessage);
              }
              Timer(
                Duration(milliseconds: 500),
                    () => _chatScrollController
                    .jumpTo(_chatScrollController.position.maxScrollExtent),
              );
              if (messages.length == 0) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return RefreshIndicator(
                onRefresh: () {
                  // Load extra messages
                  return _getAndLoadMessageHistory(_loadedDays + 1);
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: ListView.separated(
                    physics: const AlwaysScrollableScrollPhysics(),
                    itemCount: messages.length,
                    controller: _chatScrollController,
                    itemBuilder: (context, i) {
                      if (messages[i].imageURL != "") {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${messages[i].user} - ${messages[i].date}",
                              style: TextStyle(fontSize: 12.0),
                            ),
                            Text(
                              messages[i].content,
                              style: TextStyle(fontSize: 17.0),
                            ),
                            Image.network(
                              messages[i].imageURL,
                            ),
                          ],
                        );
                      }

                      if ((i >= 1 &&
                              messages[i].user != messages[i - 1].user) ||
                          i == 0) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${messages[i].user} - ${messages[i].date}",
                              style: TextStyle(fontSize: 12.0),
                            ),
                            Text(
                              messages[i].content,
                              style: TextStyle(fontSize: 17.0),
                            )
                          ],
                        );
                      }
                      return Text(
                        messages[i].content,
                        style: TextStyle(fontSize: 17.0),
                      );
                    },
                    separatorBuilder: (BuildContext context, int i) {
                      if (i + 1 < messages.length &&
                          messages[i].user != messages[i + 1].user) {
                        return Divider();
                      } else {
                        return SizedBox(
                          height: 1,
                        );
                      }
                    },
                  ),
                ),
              );
            },
          ),
        ),
        Container(
            alignment: Alignment.bottomCenter,
            decoration: BoxDecoration(
                border:
                    Border(top: BorderSide(color: Colors.black, width: 4.0)),
                color: Colors.white),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: _chatBoxController,
                    style: TextStyle(
                      color: Colors.black87,
                    ),
                  ),
                ),
                SizedBox(
                  width: 4.0,
                ),
                RaisedButton(
                  onPressed: sendMessage,
                  child: Text("Verzend"),
                )
              ],
            ))
      ],
    );
  }

  void sendMessage() {
    String message = _chatBoxController.text;
    if (message == "") {
      snackbar("Voer een bericht in");
      return;
    }
    snackbar("Je bericht wordt verzonden");
    _chatBoxController.clear();
    _chatService.sendMessage(message);
  }

  void snackbar(String text) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(text),
      duration: Duration(
        seconds: 1,
      ),
    ));
  }

  Future<void> _getAndLoadMessageHistory(int from) async {
    if (from < 1) {
      throw Exception("from has to be greater than 1");
    }
    if (messages.length > 1 && messages[0].id == 1) {
      // All messages have been loaded
      return;
    }
    var messageHistory = await _chatService.getMessageHistory(from, from - 1);
    if (messageHistory.length == 0) {
      return _getAndLoadMessageHistory((from + 1));
    }
    _loadedDays = from;
    setState(() {
      messages = messageHistory + messages;
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    _getAndLoadMessageHistory(1);
    Timer(
      Duration(milliseconds: 500),
      () => _chatScrollController
          .jumpTo(_chatScrollController.position.maxScrollExtent),
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    _chatBoxController.dispose();
    _chatService.closeWebsocket();
    super.dispose();
  }
}


