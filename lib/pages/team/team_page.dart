import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutterdummy/services/romereis_http.dart';
import 'package:flutterdummy/variables.dart';
import 'chat_page.dart';

class TeamPage extends StatefulWidget {
  TeamPage({Key key, @required this.teamId, this.teamName, this.teamCategory}) : super(key: key);
  final int teamId;
  final String teamName;
  final String teamCategory;

  @override
  TeamPageState createState() => TeamPageState();
}

class TeamPageState extends State<TeamPage> {

  RomereisHttp _romereisHttp;

  @override
  Widget build(BuildContext context) {

    _romereisHttp = RomereisHttp(context);

    return DefaultTabController(
      length: 2,
      child: Scaffold(
          appBar: AppBar(
            title: Text("${widget.teamName} (${widget.teamCategory})"),
            bottom: TabBar(tabs: <Widget>[
              Tab(icon: Icon(Icons.people),),
              Tab(icon: Icon(Icons.chat),),
            ],),
          ),
          body: TabBarView(
            children: <Widget>[
              _createTeamListWidget(),
              _createChatWidget(),
            ],
          ),
      ),
    );
  }

  Widget _createTeamListWidget() {
    return FutureBuilder(
      future: _romereisHttp.get('$augustusURL/getTeamMembers?sessionId=${credentialsManager.getSessionId()}&teamId=${widget.teamId}'),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data != null) {
            List<dynamic> teamMembers = List.from(jsonDecode(snapshot.data.body));
            if (teamMembers.length < 1) {
              return Center(
                child: Text(
                  "Deze groep heeft geen leden",
                  style: TextStyle(
                      fontStyle: FontStyle.italic
                  ),
                )
              );
            }
            return ListView.builder(
              itemCount: teamMembers.length,
              itemBuilder: (context, i) {
                return ListTile(
                  title: Text(teamMembers[i]["name"]),
                );
              },
            );
          } else {
            return Center(
              child: Text("Herlaad deze pagina a.u.b"),
            );
          }
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _createChatWidget() {
    return FutureBuilder(
      future: _romereisHttp.get('$augustusURL/getTeamMembers?sessionId=${credentialsManager.getSessionId()}&teamId=${widget.teamId}'),
      builder: (context, snapshot) {
        if (snapshot.data != null) {
          bool partOfTeam = false;
          for (var element in List.from(jsonDecode(snapshot.data.body))){
            if(element["id"] == credentialsManager.getUserId()) {
              partOfTeam = true;
              break;
            }
          }
          if (partOfTeam) {
            return ChatPage(teamId: widget.teamId.toString(),);
          } else {
            return Center(child: Text("Je bent geen onderdeel van deze groep.\nJe kan daarom ook niet deelnemen aan de chat.", textAlign: TextAlign.center,));
          }
        } else {
          return Center(
            child: Text("Herlaad deze pagina a.u.b"),
          );
        }
      },
    );
  }
}
