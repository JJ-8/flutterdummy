import 'package:flutter/material.dart';
import 'package:after_layout/after_layout.dart';
import 'package:flutterdummy/variables.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';

import 'login_page.dart';

class SendNotificationPage extends StatefulWidget {
  @override
  SendNotificationPageState createState() => SendNotificationPageState();
}

class SendNotificationPageState extends State<SendNotificationPage>
    with AfterLayoutMixin<SendNotificationPage> {
  final _formKey = GlobalKey<FormState>();

  Widget _submitbutton = Text("loading");

  String _message;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: 200,
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration: const InputDecoration(
                    hintText: "Aankondiging",
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return "Vul een bericht in";
                  } else if (value.length >= 255) {
                    return "Bericht is te lang";
                  } else {
                    return null;
                  }
                },
                onSaved: (value) {
                  _message = value;
                },
              ),
              SizedBox(
                height: 15,
              ),
              _submitbutton
            ],
          ),
        ),
      ),
    );
  }

  void _formSubmit() async {
    if (_formKey.currentState.validate()) {
      final oldsubmitbutton = _submitbutton;

      setState(() {
        _submitbutton = CircularProgressIndicator();
      });

      _formKey.currentState.save();

      final response = await http.post(
        "$augustusURL/sendNotification",
        body: {
          "sessionId": credentialsManager.getSessionId(),
          "message": _message
        }
      );

      if (response.statusCode == 200 && response.body == "SUCCESS") {
        _formKey.currentState.reset();
        Fluttertoast.showToast(msg: "Gelukt");
      } else if (response.body == "ERR_UNAUTHORISED") {
        Fluttertoast.showToast(msg: "Niet genoeg rechten");
      } else {
        Navigator.push<void>(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      }
      setState(() {
        _submitbutton = oldsubmitbutton;
      });
    }
  }

  @override
  void afterFirstLayout(BuildContext context) {
    setState(() {
      _submitbutton = RaisedButton(
        onPressed: _formSubmit,
        child: Text('Verstuur'),
      );
    });
  }
}
