import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:flutterdummy/models/recurringtimer.dart';
import 'package:flutterdummy/models/rr_marker.dart';
import 'package:flutterdummy/pages/photo_page.dart';
import 'package:flutterdummy/services/geotracker_service.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:sliding_panel/sliding_panel.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter/services.dart' show rootBundle;

class Geotracker extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => GeotrackerState();
}

class GeotrackerState extends State<Geotracker> {
  MapboxMapController mapController;
  PanelController _mapsPC;
  Symbol _selectedSymbol;
  GeotrackerService _service;
  List<Widget> _selectedPanelContent = [];
  StreamSubscription<RRMarker> websocketListener;

  void _onMapCreated(MapboxMapController controller) {
    if (kDebugMode) print("Map created");
    mapController = controller;
    mapController.onSymbolTapped.add(_onSymbolTapped);
  }

  void _onStyleLoaded() {
    if (kDebugMode) print("Style loaded");
    _initImages();
    RRMarker example = RRMarker(
      title: "Hotel",
      subtitle: "Luciani",
      position: LatLng(41.901810, 12.503973),
      type: RRMarkerType.POI,
      contentFilePath: "assets/dummy/geotracker/hotel/beschrijving.html",
      imagePaths: ["assets/dummy/geotracker/hotel/binnenkant.jpg", "assets/dummy/geotracker/hotel/buitenkant.jpg"]
    );
    mapController.addSymbol(_symbolFromMarker(example));

    _initWebsocketListener();

    // Start timer to update timeago
    RecurringTimer().interval(Duration(seconds: 15), (timer) {
      if (_selectedSymbol != null && _markerFromSymbol(_selectedSymbol).time != null) {
        setState(() {
          // Refresh state so time ago gets updated on the map
        });
      }
    });
  }


  void _onSymbolTapped(Symbol symbol) {
    if (_selectedSymbol != null && symbol != _selectedSymbol) {
      mapController.updateSymbol(_selectedSymbol, SymbolOptions(
          iconImage: _selectedSymbol.options.iconImage.replaceAll("_selected", "")
      ));
    }
    if (!symbol.options.iconImage.contains("_selected")) {
      mapController.updateSymbol(symbol, SymbolOptions(
          iconImage: symbol.options.iconImage + "_selected"
      ));

    }
    setState(() {
      _mapsPC.collapse();
      _selectedSymbol = symbol;
    });
  }


  /// Adds an asset image to the currently displayed style
  /// Taken from the mapbox_gl example project
  Future<void> addImageFromAsset(String name, String assetName) async {
    final ByteData bytes = await rootBundle.load(assetName);
    final Uint8List list = bytes.buffer.asUint8List();
    return mapController.addImage(name, list);
  }

  SymbolOptions _symbolFromMarker(RRMarker marker) {
    String iconImage;
    String iconAnchor;
    switch(marker.type) {
      case RRMarkerType.Hotel: iconImage = 'hotel';  iconAnchor = 'center'; break;
      case RRMarkerType.POI:   iconImage = 'marker'; iconAnchor = 'bottom'; break;
      case RRMarkerType.User:  iconImage = 'user';   iconAnchor = 'center'; break;
    }
    return SymbolOptions(
        geometry: marker.position,
        iconImage: iconImage,
        iconSize: marker.iconSize,
        iconAnchor: iconAnchor,
      textField: jsonEncode(marker),
      textSize: 0.0
    );
  }

  RRMarker _markerFromSymbol(Symbol symbol) {
    return RRMarker.fromJson(jsonDecode(symbol.options.textField));
  }

  @override
  void initState() {
    super.initState();
    _mapsPC = PanelController();
    timeago.setLocaleMessages('nl', timeago.NlMessages());
    timeago.setDefaultLocale('nl');
  }
  void _initImages() {
    // Original plan was to use WebPs
    // Turns out iOS doesn't handle them well
    addImageFromAsset("marker", "assets/geotracker/symbols/ic_map_marker.png");
    addImageFromAsset("marker_selected", "assets/geotracker/symbols/ic_map_marker_selected.png");
    addImageFromAsset("hotel", "assets/geotracker/symbols/ic_map_hotel.png");
    addImageFromAsset("hotel_selected", "assets/geotracker/symbols/ic_map_hotel_selected.png");
    addImageFromAsset("user", "assets/geotracker/symbols/ic_map_user.png");
    addImageFromAsset("user_selected", "assets/geotracker/symbols/ic_map_user_selected.png");
  }
  void _initWebsocketListener() {
    _service = GeotrackerService(context: context);
    _service.addListener(() {
      if (websocketListener != null) {
        // Cancel the previous subscription
        // Might save power, idk
        websocketListener.cancel();
      }
      websocketListener = _service.stream.listen((marker) => mapController.addSymbol(_symbolFromMarker(marker)));
    });
    _service.initialise();
  }

  // Generate information text widget for _generatePanelContent
  Widget _generateInformationWidget(String contentFilePath) {
    return FutureBuilder(
      future: DefaultAssetBundle.of(context).loadString(contentFilePath),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Container(
            child: HtmlWidget(
                "<BR>" + snapshot.data /* Dirty hack, but it works */
            ),
            padding: EdgeInsets.symmetric(horizontal: 10),
          );
        } else {
          return CircularProgressIndicator();
        }
      },
    );
  }

  // Generate a side-scrolling list of images for _generatePanelContent
  Widget _generateImageScroller(List<String> imagepaths) {
    final images = List<Widget>();
    imagepaths.forEach((element) {
      final assetimage = AssetImage(element);
      images.add(
        GestureDetector(
          onTap: () {
            viewImageFullscreen(assetimage);
          },
          child: Card(
            color: Colors.black,
            child: Image(image: assetimage, width: 196.3, height: 196.3,),
          ),
        ),
      );
    });
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
          children: images
      ),
    );
  }

  List<Widget> _generatePanelContent(RRMarker marker) {
    List<Widget> panelContentWidgets = List();

    if (marker.contentFilePath != null) {
      panelContentWidgets.add(
        _generateInformationWidget(marker.contentFilePath)
      );
    }

    if (marker.imagePaths != null) {
      panelContentWidgets.add(
        // Side-scrolling images
        _generateImageScroller(marker.imagePaths)
      );
    }

    // Fallback if there's nothing to be shown
    if (panelContentWidgets.length == 0) {
      panelContentWidgets.add(Text("\nNiets te zien hier!"));
    }

    return panelContentWidgets;
  }

  // Build a full-screen view for any picture passed
  // Also add a back button
  void viewImageFullscreen(AssetImage image) {
    Navigator.push<void>(
        context, MaterialPageRoute(builder: (context) =>
        Photopage(image:image, includeBackButton: true)
    ));
  }

  Widget build(BuildContext context) {
    final _selectedmarker = _selectedSymbol == null ? null : _markerFromSymbol(_selectedSymbol);
    if (_selectedmarker != null) {
      _selectedPanelContent = _generatePanelContent(_selectedmarker);
    } else {
      _selectedPanelContent = [Text("\nNiets te zien hier")];
    }
    return NotificationListener<SlidingPanelResult>(
      child: SlidingPanel(
        panelController: _mapsPC,
        initialState: InitialPanelState.dismissed,
        backdropConfig: BackdropConfig(enabled: false, shadowColor: Colors.black, dragFromBody: true),
        content: PanelContent(
          panelContent: _selectedPanelContent,
          headerWidget: PanelHeaderWidget(
            headerContent: Container(
              child: Column(
                children: [
                  Text(
                    _selectedmarker?.title ?? "",
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  Text(
                    /* If the selected marker has a subtitle, display it. If it doesn't and it has a timestamp, display the timestamp in a "x hours/minutes/seconds ago" format. */
                      _selectedmarker?.subtitle ?? (_selectedmarker?.time == null ? "" : timeago.format(_selectedmarker.time))
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
              ),
              width: double.infinity,
            ),
            options: PanelHeaderOptions(
              centerTitle: false,
              elevation: 4,
              forceElevated: true,
              primary: false,
            ),
            onTap: () => _mapsPC.currentState == PanelState.closed
                ? _mapsPC.expand()
                : _mapsPC.close(),
            decoration:
            PanelDecoration(padding: EdgeInsets.all(16)),
          ),
          collapsedWidget: PanelCollapsedWidget( /* This widget needs to exist so autoSizing works correctly */
            collapsedContent: SizedBox(height: 1,)
          ),
          bodyContent: MapboxMap(
            accessToken: DotEnv().env['MAPBOX_ACCESS_TOKEN'],
            onMapCreated: _onMapCreated,
            onStyleLoadedCallback: _onStyleLoaded,
            initialCameraPosition: const CameraPosition(
                target: LatLng(41.901810, 12.503973), zoom: 15.0),
            myLocationEnabled: true,
            myLocationRenderMode: MyLocationRenderMode.COMPASS,
            myLocationTrackingMode: MyLocationTrackingMode.Tracking,
          ),
        ),
        isTwoStatePanel: false,
        snapping: PanelSnapping.forced,
        isDraggable: true,
        // when panel closes, dont allow re-opening by drags, and
        // send and throw default results.
        size: PanelSize(closedHeight: 0.0, expandedHeight: 1.0),
        autoSizing: PanelAutoSizing(
            autoSizeExpanded: false,
            useMinExpanded: true,
            headerSizeIsClosed: false,
            autoSizeCollapsed: true
        ),
        duration: Duration(milliseconds: 500),
        dragMultiplier: 2.0,
        backPressBehavior: BackPressBehavior.COLLAPSE_CLOSE_PERSIST,
        onPanelStateChanged: (panelState) {
          if (panelState == PanelState.closed || panelState == PanelState.dismissed) {
            mapController.updateSymbol(_selectedSymbol, SymbolOptions(
              iconImage: _selectedSymbol.options.iconImage.replaceAll("_selected", "")
            ));
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    mapController?.onSymbolTapped?.remove(_onSymbolTapped);
    websocketListener.cancel();
    super.dispose();
  }
}
