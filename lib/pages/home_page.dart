import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:flutter/rendering.dart';
import 'package:flutterdummy/services/dynamic_content_manager.dart';
import 'package:flutterdummy/services/notification_history_service.dart';

class Homepage extends StatelessWidget {
  final void Function(String type, String key, String title) loaderfunction;

  const Homepage({this.loaderfunction});

  @override
  Widget build(BuildContext context) {

    final dynamicContentManager = DynamicContentManager();
    final programmefuture = dynamicContentManager.fetchDynamicContent("programme");

    final notificationsHistoryService = NotificationsHistoryService(context);

    return Center(
      child: ListView(
        children: <Widget>[
          Card(
              margin: EdgeInsets.all(8.0),
              child: Container(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text("Programma"),
                      Divider(),
                      FutureBuilder(
                        future: programmefuture,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            return Html(
                              data: snapshot.data,
                              style: {
                                'i': Style(
                                  margin: EdgeInsets.zero,
                                  padding: EdgeInsets.zero,
                                ),
                                'p': Style(
                                  margin: EdgeInsets.zero,
                                  padding: EdgeInsets.zero,
                                ),
                                'body': Style(
                                    margin: EdgeInsets.zero,
                                    padding: EdgeInsets.zero),
                                'head': Style(
                                  margin: EdgeInsets.zero,
                                  padding: EdgeInsets.zero,
                                ),
                              },
                            );
                          } else {
                            return Center(
                              child: Padding(
                                padding: EdgeInsets.all(8.0),
                                child: CircularProgressIndicator(),
                              ),
                            );
                          }
                        },
                      )
                    ],
                  ))),
          Card(
            margin: EdgeInsets.all(8.0),
            child: Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Aankondigingen",
                  ),
                  Divider(),
                  FutureBuilder(
                    future: notificationsHistoryService.getNotifications(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {

                        List<Widget> children = List();
                        // The total amount of notifications sent is below 5
                        // Display all notifications
                        if (notificationsHistoryService.notifications.length < 5) {
                          notificationsHistoryService.notifications.reversed.forEach((element) {
                            children.add(ListTile(
                              title: Text(element.message),
                              subtitle: Text(
                                  "${element.notiftime} · ${element.username}"),
                            ));
                          });
                        } else {
                          // The total amount of notifications sent is more than five
                          // Display only the first five notifications
                          for (var i = 0; i < 5; i++) {
                            final element = notificationsHistoryService.notifications.reversed.elementAt(i);
                            children.add(ListTile(
                              title: Text(element.message),
                              subtitle: Text(
                                  "${element.notiftime} · ${element.username}"),
                            ));
                          }
                          children.add(
                            FlatButton(
                              child: Text(
                                "en ${notificationsHistoryService.notifications.length - 5} meer",
                                style: TextStyle(
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                              onPressed: () {
                                loaderfunction(
                                    "special", "notifications", "Notificaties");
                              },
                            ),
                          );
                        }

                        return Column(
                          children: children,
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
