import 'dart:async';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutterdummy/pages/geotracker.dart';
import 'package:flutterdummy/services/dynamic_content_manager.dart';
import 'package:flutterdummy/pages/team/chat_page.dart';
import 'package:flutterdummy/pages/team/team_list.dart';
import 'package:flutterdummy/pages/dynamic_content_page.dart';
import 'package:flutterdummy/pages/notifications_page.dart';
import 'package:flutterdummy/pages/photo_page.dart';
import 'package:flutterdummy/pages/send_notification_page.dart';
import 'package:flutterdummy/services/user_service.dart';
import 'package:flutterdummy/variables.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'layout/android_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'pages/login_page.dart';
import 'pages/home_page.dart';
import 'package:xml/xml.dart';
import 'pages/text_page.dart';
import 'package:after_layout/after_layout.dart';
import 'package:flutter/foundation.dart';
import 'package:location/location.dart';

void main() async {
  await DotEnv().load('.env');

  // Run the whole app in a zone to capture all uncaught errors.
  runZonedGuarded(
    () => runApp(DummyFlutterApp()),
    (error, stackTrace) {
      if (kReleaseMode) {
        try {
          sentry.captureException(
            exception: error,
            stackTrace: stackTrace,
          );
          print('Error sent to sentry.io: $error');
        } catch (e) {
          print('Sending report to sentry.io failed: $e');
          print('Original error: $error');
        }
      }
    },
  );
}

class DummyFlutterApp extends StatelessWidget {
  @override
  Widget build(context) {
    return MaterialApp(
      title: 'Dummy',
      theme: ThemeData(
          brightness: Brightness.light,
          primarySwatch: Colors.grey,
          primaryColor: Colors.black,
          dividerColor: Colors.black54,
          iconTheme: IconThemeData(color: Colors.grey[800])),
      darkTheme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.dark,
          iconTheme: IconThemeData(color: Colors.white),
          accentColor: Colors.grey,
          dividerColor: Colors.white60,
          buttonColor: Colors.grey),
      home: MainPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with AfterLayoutMixin<MainPage> {
  Future<XmlDocument> _getMenuXML() async =>
      XmlDocument.parse(await rootBundle.loadString('assets/menu.xml'));

  String _title = "Romereis";

  /* No need to initialise the Homepage here, this is done in initialise() */
  Widget _body = Center(child: CircularProgressIndicator());

  /*  ###
   *  App component loader functions
      ### */
  void _loadAppComponent(String type, String key, String title) {
    setState(() {
      _title = title;
    });
    if (type == "file") {
      _loadFile(key);
    } else if (type == "special") {
      _loadSpecial(key);
    }
  }

  Future<void> _loadSpecial(String key) async {
    if (key == "geotracker" && !kIsWeb) {
      final location = Location();
      final hasPermissions = await location.hasPermission();
      if (hasPermissions != PermissionStatus.granted) {
        if (await location.requestPermission() != PermissionStatus.granted) {
          Fluttertoast.showToast(
            msg: "Geef eerst locatietoegang",
          );
          return;
        }
      }
    }
    setState(() {
      // if/else in case info or programme should be loaded
      if (["info", "programme"].contains(key)) {
        final dynamicContentManager = DynamicContentManager();
        Future<String> future = dynamicContentManager.fetchDynamicContent(key);
        _body = DynamicContentPage(
          contentfuture: future,
        );
        return;
      }
      // switch for the rest of cases
      switch (key) {
        case "notifications":
          _body = NotificationsPage();
          break;
        case "sendNotification":
          _body = SendNotificationPage();
          break;
        case "teams":
          _body = TeamList();
          break;
        case "globalchat":
          _body = ChatPage(
            teamId: 'globalchat',
          );
          break;
        case "geotracker":
          _body = Geotracker();
          break;
      }
    });
  }

  void _loadFile(String key) {
    final extension = key.split(".")[key.split(".").length - 1];
    if (extension == "html" || extension == "html") {
      _loadHTML(key);
    } else if (extension == "webp" ||
        extension == "png" ||
        extension == "jpg" ||
        extension == "jpeg") {
      _loadImage(key);
    }
  }

  _loadHTML(String filename) {
    final Future<String> filefuture = rootBundle.loadString(filename);
    setState(() {
      _body = TextPage(filefuture: filefuture);
    });
  }

  _loadImage(String filename) {
    final image = AssetImage(filename);
    setState(() {
      _body = Photopage(
        image: image,
      );
    });
  }

  /*  ###
   *  End app component loader functions
      ### */

  @override
  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
      ),
      drawer: AndroidDrawer(
        xmlfuture: _getMenuXML(),
        loaderfunction: _loadAppComponent,
      ),
      body: _body,
    );
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    // Await CredentialsManager's Preference Future
    // This makes sure that _sessionId, _userId, etc are initialised
    await credentialsManager.prefsFuture;

    bool sessionValidity = await UserService(context).validateSession();

    if (!sessionValidity) {
      _openLoginScreen();
    } else {
      _initialise();
    }
  }

  void _initialise() {
    UserService(context).checkIfAdmin().then((isAdmin) {
      credentialsManager.setAdmin(isAdmin);
    });

    if (kDebugMode) {
      print("SessionId: ${credentialsManager.getSessionId()}");
    }

    final dynamicContentManager = DynamicContentManager();
    dynamicContentManager.fetchGeneralInfo();
    dynamicContentManager.fetchTeams();
    setState(() {
      _body = Homepage(
        loaderfunction: _loadAppComponent,
      );
    });
  }

  void _openLoginScreen() {
    Navigator.push<void>(
            context, MaterialPageRoute(builder: (context) => LoginScreen()))
        .then((value) {
      _initialise();
    });
  }
}
