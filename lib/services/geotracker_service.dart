import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutterdummy/models/rr_marker.dart';
import 'package:flutterdummy/services/websocket_service.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

import '../variables.dart';

class GeotrackerService extends WebsocketService {

  GeotrackerService({@required context}): super(context);

  @override
  Stream<RRMarker> createStream() {
    StreamController<RRMarker> controller;
    StreamSubscription source;

    void processWSMessages() {
      source = channel.stream.listen( (chunk) {
        if (chunk is String) {
          if (kDebugMode) print("Received WebSocket message: $chunk");

          Map<String, dynamic> jsonMessage = jsonDecode(chunk);
          String channel = jsonMessage["channel"];
          if (channel == "locations") {
            final username = jsonMessage["user"];
            final latitude = jsonMessage["lat"].toString();
            final longitude = jsonMessage["lon"].toString();
            final time = DateTime.parse(jsonMessage["time"]);
            controller.add(new RRMarker(
              title: username,
              position: LatLng(double.parse(latitude), double.parse(longitude)),
              time: time,
              type: RRMarkerType.User,
            ));
          }
        }
      } );
    }

    void cancelProcessing() {
      source.cancel();
      controller.close();
    }

    controller = StreamController<RRMarker>.broadcast(
      onListen: processWSMessages,
      onCancel: cancelProcessing
    );

    return controller.stream;
  }
  @override
  void onConnect() {
    this.channel.sink.add("{\"channel\": \"getCachedLocations\", \"sessionId\": \"${credentialsManager.getSessionId()}\"}");
  }
}