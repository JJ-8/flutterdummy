import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterdummy/models/recurringtimer.dart';
import 'package:flutterdummy/pages/login_page.dart';
import 'package:web_socket_channel/io.dart';

import '../variables.dart';

class WebsocketService extends ChangeNotifier {
  IOWebSocketChannel channel;
  Stream stream;
  final BuildContext context;

  bool internetConnection = true;

  WebsocketService(this.context);

  void initialise() async {
    if (kDebugMode) print("Connecting to websocket: $juliusURL");
    final isOnline = await testInternet();
    if (isOnline) {
      this.channel = IOWebSocketChannel.connect(juliusURL);
      this.stream = createStream();
      // Notify listeners that they can start expecting messages
      notifyListeners();
      _finalsetup();
    }
    setupInternetTimer();
  }

  // Function to test if there's a connection to Internet
  Future<bool> testInternet() async {
    try {
      final result = await InternetAddress.lookup(augustusURL.replaceAll('https://', ''));
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      return false;
    }
  }

  // Sets up a timer that checks for internet once in a while
  void setupInternetTimer() {
    RecurringTimer().interval(Duration(seconds: 15), (timer) async {
      final isThereInternet = await testInternet();
      if (isThereInternet) {
        if (internetConnection == false) {
          if (kDebugMode) print("Connected to the internet, reconnecting to websocket");
          reconnect();
          internetConnection = true;
        }
      } else {
        //Disconnected from the internet
        if (kDebugMode) print("Disconnected from the internet");
        if (internetConnection == true) {
          // Close the websocket
          closeWebsocket();
        }
        internetConnection = false;
      }
    });
  }

  void closeWebsocket() {
    channel.sink.close();
  }

  void reconnect() async {
    if (kDebugMode) print("Connection lost, reconnecting");
    // Reconnect
    closeWebsocket();
    channel = IOWebSocketChannel.connect(juliusURL);
    stream = createStream();
    notifyListeners();
    _finalsetup();
  }

  void _finalsetup() {
    if (kDebugMode) print("Sending {\"channel\": \"authentication\", \"sessionId\": \"${credentialsManager.getSessionId()}\"}");
    channel.sink.add(
        "{\"channel\": \"authentication\", \"sessionId\": \"${credentialsManager.getSessionId()}\"}");
    onConnect();
    channel.sink.done.then((error) => {
      if (internetConnection) {
        reconnect()
      }
    });
  }
  
  void onConnect(){
   //This function should be overridden by the class using it 
  }

  // Generic method to create a stream
  // Should be overriden in the specific service
  Stream createStream() {
    StreamController<String> controller;
    StreamSubscription source;

    void processWSMessages() {
      source = channel.stream.listen( (chunk) {
        if (chunk is String) {
          controller.add(chunk);
        }
      } );
    }

    void cancelProcessing() {
      source.cancel();
    }

    controller = StreamController<String>(
        onListen: processWSMessages,
        onCancel: cancelProcessing
    );

    return controller.stream;
  }

  /*
    Open login screen function
   */

  void openLoginScreen() {
    if (!loginScreenOpened) {
      closeWebsocket();
      Navigator.push<void>(context, MaterialPageRoute(builder: (context) => LoginScreen())).then((value) {
        reconnect();
      });
    }
  }
}